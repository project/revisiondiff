<?php

namespace Drupal\revisiondiff\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\diff\DiffLayoutInterface;
use Drupal\diff\DiffLayoutManager;
use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsFormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity reference revision diff' formatter.
 *
 * @FieldFormatter(
 *   id = "revisiondiff",
 *   label = @Translation("Diff to previous revision"),
 *   description = @Translation("Display the diff of the referenced entity revision and the previous revision (that the current user has access to)."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class RevisionDiffFieldFormatter extends EntityReferenceRevisionsFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * An array of counters for the recursive rendering protection.
   *
   * Each counter takes into account all the relevant information about the
   * field and the referenced entity that is being rendered.
   *
   * @var array
   *
   * @see \Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter::viewElements()
   */
  protected static $recursiveRenderDepth = [];

  /**
   * The number of times this formatter allows rendering the same entity.
   *
   * @var int
   */
  const RECURSIVE_RENDER_LIMIT = 20;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field diff layout plugin manager service.
   *
   * @var \Drupal\diff\DiffLayoutManager
   */
  protected $diffLayoutManager;

  /**
   * Constructs a RevisionDiffFieldFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\diff\DiffLayoutManager $diffLayoutManager
   *   The diff layout manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LoggerChannelFactoryInterface $loggerFactory, EntityTypeManagerInterface $entityTypeManager, DiffLayoutManager $diffLayoutManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->loggerFactory = $loggerFactory;
    $this->entityTypeManager = $entityTypeManager;
    $this->diffLayoutManager = $diffLayoutManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.diff.layout')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'diff_layout' => 'visual_inline',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['diff_layout'] = [
      '#type' => 'select',
      '#options' => $this->diffLayoutManager->getPluginOptions(),
      '#title' => $this->t('Diff layout'),
      '#default_value' => $this->getSetting('diff_layout'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $diffLayout = $this->getSetting('diff_layout');
    $diffLayoutLabels = $this->diffLayoutManager->getPluginOptions();
    $diffLayoutLabel = $diffLayoutLabels[$diffLayout] ??
      $this->t('Broken (@diff_layout)', ['@diff_layout' => $diffLayout]);
    $summary[] = $this->t('Diff layout: @diff_layout', ['@diff_layout' => $diffLayoutLabel]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    try {
      /** @var \Drupal\diff\DiffLayoutInterface $diffLayoutHandler */
      $diffLayoutHandler = $this->diffLayoutManager->createInstance($this->getSetting('diff_layout'));
    }
    catch (PluginException $e) {
      // Silently ignore this.
      return [];
    }

    // @see ::isApplicable
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    $elements = [];
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      if ($this->isRecursion($items, $entity)) {
        return $elements;
      }
      $previousRevision = $this->getPreviousRevision($entity);
      $diff = $this->buildDiff($previousRevision, $entity, $diffLayoutHandler);
      if ($diff) {
        $elements[$delta] = $diff;
      }
    }
    // Note that cacheability metadata will be added in.
    // @see \Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase::getEntitiesToView
    // For revision access metadata, see comment in ::loadPreviousRevision.
    return $elements;
  }

  /**
   * Check for recusrion.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The items.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   If we have recursion.
   *
   *   Copied from @see \Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter::viewElements
   */
  protected function isRecursion(FieldItemListInterface $items, EntityInterface $entity): bool {
    // Due to render caching and delayed calls, the viewElements() method
    // will be called later in the rendering process through a '#pre_render'
    // callback, so we need to generate a counter that takes into account
    // all the relevant information about this field and the referenced
    // entity that is being rendered.
    $recursive_render_id = $items->getFieldDefinition()->getTargetEntityTypeId()
      . $items->getFieldDefinition()->getTargetBundle()
      . $items->getName()
      // We include the referencing entity, so we can render default images
      // without hitting recursive protections.
      . $items->getEntity()->id()
      . $entity->getEntityTypeId()
      . $entity->id();

    if (isset(static::$recursiveRenderDepth[$recursive_render_id])) {
      static::$recursiveRenderDepth[$recursive_render_id]++;
    }
    else {
      static::$recursiveRenderDepth[$recursive_render_id] = 1;
    }

    // Protect ourselves from recursive rendering.
    $isRecursion = static::$recursiveRenderDepth[$recursive_render_id] > static::RECURSIVE_RENDER_LIMIT;
    if ($isRecursion) {
      $this->loggerFactory->get('entity')
        ->error('Recursive rendering detected when rendering entity %entity_type: %entity_id, using the %field_name field on the %bundle_name bundle. Aborting rendering.', [
          '%entity_type' => $entity->getEntityTypeId(),
          '%entity_id' => $entity->id(),
          '%field_name' => $items->getName(),
          '%bundle_name' => $items->getFieldDefinition()->getTargetBundle(),
        ]);
    }
    return $isRecursion;
  }

  /**
   * Build a single diff.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $leftRevision
   *   The left revision.
   * @param \Drupal\Core\Entity\ContentEntityInterface $rightRevision
   *   The right revision.
   * @param \Drupal\diff\DiffLayoutInterface $diffLayoutHandler
   *   The diff layout handler.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @see \Drupal\diff\Controller\PluginRevisionController::compareEntityRevisions
   */
  protected function buildDiff(ContentEntityInterface $leftRevision, ContentEntityInterface $rightRevision, DiffLayoutInterface $diffLayoutHandler) {
    // Whyever the handler needs this in the first place, give it.
    $entity = $this->loadDefaultRevision($leftRevision);
    $build = $diffLayoutHandler->build($leftRevision, $rightRevision, $entity);
    if (
      isset($build['diff']['#type'])
      && $build['diff']['#type'] === 'table'
      && empty($build['diff']['#rows'])
    ) {
      $build = [];
    }
    else {
      $build['diff']['#prefix'] = '<div class="diff-responsive-table-wrapper">';
      $build['diff']['#suffix'] = '</div>';
      $build['diff']['#attributes']['class'][] = 'diff-responsive-table';
      $build['#attached']['library'][] = 'diff/diff.general';
      $build['header']['#access'] = FALSE;
      $build['controls']['#access'] = FALSE;
    }
    return $build;
  }

  /**
   * Load default revision.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $leftRevision
   *   Some entity revision.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadDefaultRevision(ContentEntityInterface $leftRevision) {
    $entityType = $leftRevision->getEntityType();
    $entityStorage = $this->entityTypeManager->getStorage($entityType->id());
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $entityStorage->load($leftRevision->id());
    return $entity;
  }

  /**
   * Load the previous revision of an entity revision.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The current entity revision.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The previous entity revision.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getPreviousRevision(ContentEntityInterface $entity) {
    $revisionKey = $entity->getEntityType()->getKey('revision');
    $revisionable = $entity->get($revisionKey)
      ->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->isRevisionable();
    if ($revisionable) {
      $previousRevision = $this->loadPreviousRevision($entity);
    }
    else {
      // @todo Remove this once we have https://www.drupal.org/project/drupal/issues/2766135
      $previousRevision = $this->loadPreviousRevisionLegacy($entity);
    }

    if (!$previousRevision) {
      $previousRevision = $this->createEmptyRevision($entity);
    }
    return $previousRevision;
  }

  /**
   * Load previous revision via filtered query.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException

   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadPreviousRevision(ContentEntityInterface $entity) {
    // We keep entity access check. Note that this can lead to diff depending
    // on user permissions.
    // If entity access check adds some cache contexts, in the lack of a proper
    // API for this, cacheability metadata is added to the current render
    // context, which is good enough here.
    // @see https://www.drupal.org/project/drupal/issues/2876258
    $entityType = $entity->getEntityType();
    $entityStorage = $this->entityTypeManager->getStorage($entityType->id());
    $query = $entityStorage->getQuery()
      ->allRevisions()
      ->condition($entityType->getKey('id'), $entity->id())
      ->condition($entityType->getKey('revision'), $entity->getLoadedRevisionId(), '<')
      ->pager(1)
      ->sort($entityType->getKey('revision'), 'DESC');
    /** @var \Drupal\Core\Entity\ContentEntityInterface[] $result */
    $result = $query->execute();
    if ($result) {
      $revisionId = key($result);
      /** @var \Drupal\Core\Entity\ContentEntityInterface $previousRevision */
      $previousRevision = $entityStorage->loadRevision($revisionId);
      $previousRevision = $previousRevision->getTranslation($entity->language()
        ->getId());
    }
    else {
      $previousRevision = NULL;
    }
    return $previousRevision;
  }

  /**
   * Load previous revision via PHP-postfiltered query.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadPreviousRevisionLegacy(ContentEntityInterface $entity) {
    // We keep entity access check. Note that this can lead to diff depending
    // on user permissions.
    // If entity access check adds some cache contexts, in the lack of a proper
    // API for this, cacheability metadata is added to the current render
    // context, which is good enough here.
    // @see https://www.drupal.org/project/drupal/issues/2876258
    $entityType = $entity->getEntityType();
    $entityStorage = $this->entityTypeManager->getStorage($entityType->id());
    $query = $entityStorage->getQuery()
      ->allRevisions()
      ->condition($entityType->getKey('id'), $entity->id())
      ->sort($entityType->getKey('revision'), 'DESC');
    /** @var \Drupal\Core\Entity\ContentEntityInterface[] $result */
    $result = $query->execute();
    if ($result) {
      $revisionIds = array_keys($result);
      $currentRevisionIdIndex = array_search($entity->getRevisionId(), $revisionIds);
      if ($currentRevisionIdIndex !== FALSE && isset($revisionIds[$currentRevisionIdIndex + 1])) {
        $revisionId = $revisionIds[$currentRevisionIdIndex + 1];
        /** @var \Drupal\Core\Entity\ContentEntityInterface $previousRevision */
        $previousRevision = $entityStorage->loadRevision($revisionId);
        $previousRevision = $previousRevision->getTranslation($entity->language()->getId());
      }
      else {
        $previousRevision = NULL;
      }
    }
    else {
      $previousRevision = NULL;
    }
    return $previousRevision;
  }

  /**
   * Create an empty revision.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\RevisionLogInterface

   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function createEmptyRevision(ContentEntityInterface $entity) {
    $entityType = $entity->getEntityType();
    $entityStorage = $this->entityTypeManager->getStorage($entityType->id());
    // If the query result is empty, we assume that we see the first revision
    // (that the current user has access to) and construct an empty entity.
    $values = [$entityType->getKey('revision') => 0] +
      array_intersect_key($entity->toArray(), array_flip([
        $entityType->getKey('id'),
        $entityType->getKey('bundle'),
        $entityType->getKey('langcode'),
        $entityType->getKey('uuid'),
      ]));
    $previousRevision = $entityStorage->create($values);
    if ($entity instanceof RevisionLogInterface) {
      /** @var \Drupal\Core\Entity\RevisionLogInterface $previousRevision */
      $previousRevision->setRevisionUser($entity->getRevisionUser());
    }
    return $previousRevision;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This formatter is only available for content entity target types.
    $target_type = $field_definition->getFieldStorageDefinition()->getSetting('target_type');
    $targetEntityType = \Drupal::entityTypeManager()->getDefinition($target_type);
    return $targetEntityType instanceof ContentEntityTypeInterface;
  }

}
